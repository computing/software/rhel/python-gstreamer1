%define release 1.1
%global gstreamer1_version 1.16.1
%global with_python3 1
%global debug_package %{nil}

Name:           python-gstreamer1
Version:        1.16.1
Release:        %{release}%{?dist}
Summary:        Python bindings for GStreamer

Group:          Development/Languages
License:        LGPLv2+
URL:            http://gstreamer.freedesktop.org/
Source:         http://gstreamer.freedesktop.org/src/gst-python/gst-python-%{version}.tar.xz

%if 0%{?with_python3}
BuildRequires:  python3-devel
%endif # if with_python3
BuildRequires:  pkgconfig
BuildRequires:  gstreamer1-devel >= %{gstreamer1_version}
BuildRequires:  pygobject3-devel

# For the benefit of people migrating from the GStreamer-0.10 package,
# which was called gstreamer-python

%global _description\
This module contains PyGObject overrides to make it easier to write\
applications that use GStreamer 1.x in Python.

%description %_description

%if 0%{?with_python3}
%package -n python%{python3_pkgversion}-gstreamer1
Summary:        Python bindings for GStreamer
Provides:       python3-gstreamer1
Requires:       python%{python3_pkgversion}-gobject%{?_isa}
Requires:       gstreamer1%{?_isa} >= %{gstreamer1_version}

%description -n python%{python3_pkgversion}-gstreamer1
This module contains PyGObject overrides to make it easier to write
applications that use GStreamer 1.x in Python 3.

%endif # with_python3

%prep
%setup -q -n gst-python-%{version}

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'
%endif # with_python3

find -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python2}|'

%build
%if 0%{?with_python3}
pushd %{py3dir}
%configure PYTHON=%{__python3}
make %{?_smp_mflags}
popd
%endif # with_python3

%install
%if 0%{?with_python3}
pushd %{py3dir}
make install DESTDIR=$RPM_BUILD_ROOT
popd
%endif # with_python3

find $RPM_BUILD_ROOT -type f -name "*.la" -exec rm -f {} ';'

%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-gstreamer1
%license COPYING
%doc AUTHORS ChangeLog NEWS README
%{python3_sitearch}/gi/overrides/*
%{_libdir}/gstreamer-1.0/libgstpython.*.so

%endif # with_python3

%changelog
* Mon Nov 14 2022 Adam Mercer <adam.mercer@ligo.org> = 1.16.1-1.1
- disable debug package

* Mon May 02 2022 Alexander Pace <alexander.pace@ligo.org> - 1.16.1-1
- upgrade gstreamer version
- deprecate python2

* Mon Apr 27 2020 Alexander Pace <alexander.pace@ligo.org> - 1.14.4-1.4
- updated to provide python36-gstreamer1

* Tue Oct 23 2018 Adam Mercer <adam.mercer@ligo.org> - 1.14.4-1.2
- provide and obsolete python-gstreamer1

* Tue Oct 23 2018 Adam Mercer <adam.mercer@ligo.org> - 1.14.4-1.1
- rebuild for EL7

* Wed Oct 03 2018 Wim Taymans <wtaymans@redhat.com> - 1.14.4-1
- Update to 1.14.4

* Tue Sep 18 2018 Wim Taymans <wtaymans@redhat.com> - 1.14.3-1
- Update to 1.14.3

* Mon Jul 23 2018 Wim Taymans <wtaymans@redhat.com> - 1.14.2-1
- Update to 1.14.2

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.14.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Tue Jun 19 2018 Miro Hrončok <mhroncok@redhat.com> - 1.14.1-2
- Rebuilt for Python 3.7

* Mon May 21 2018 Wim Taymans <wtaymans@redhat.com> - 1.14.1-1
- Update to 1.14.1

* Tue Mar 20 2018 Wim Taymans <wtaymans@redhat.com> - 1.14.0-1
- Update to 1.14.0

* Wed Mar 14 2018 Wim Taymans <wtaymans@redhat.com> - 1.13.91-1
- Update to 1.13.91

* Mon Mar 05 2018 Wim Taymans <wtaymans@redhat.com> - 1.13.90-1
- Update to 1.13.90

* Thu Feb 22 2018 Wim Taymans <wtaymans@redhat.com> - 1.13.1-1
- Update to 1.13.1
- The plugin has been renamed

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.12.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Mon Dec 11 2017 Wim Taymans <wtaymans@redhat.com> - 1.12.4-1
- Update to 1.12.4

* Tue Sep 19 2017 Wim Taymans <wtaymans@redhat.com> - 1.12.3-1
- Update to 1.12.3

* Sat Aug 19 2017 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl> - 1.12.2-4
- Python 2 binary package renamed to python2-gstreamer1
  See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.12.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.12.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Mon Jul 17 2017 Wim Taymans <wtaymans@redhat.com> - 1.12.2-1
- Update to 1.12.2

* Tue Jun 20 2017 Wim Taymans <wtaymans@redhat.com> - 1.12.1-1
- Update to 1.12.1

* Wed May 10 2017 Wim Taymans <wtaymans@redhat.com> - 1.12.0-1
- Update to 1.12.0

* Fri Apr 28 2017 Wim Taymans <wtaymans@redhat.com> - 1.11.91-1
- Update to 1.11.91

* Tue Apr 11 2017 Wim Taymans <wtaymans@redhat.com> - 1.11.90-1
- Update to 1.11.90

* Fri Feb 24 2017 Wim Taymans <wtaymans@redhat.com> - 1.11.2-1
- Update to 1.11.2

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.11.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Fri Jan 13 2017 Wim Taymans <wtaymans@redhat.com> - 1.11.1-1
- Update to 1.11.1

* Tue Dec 13 2016 Stratakis Charalampos <cstratak@redhat.com> - 1.10.2-2
- Rebuild for Python 3.6

* Mon Dec 05 2016 Wim Taymans <wtaymans@redhat.com> - 1.10.2-1
- Update to 1.10.2

* Mon Nov 28 2016 Wim Taymans <wtaymans@redhat.com> - 1.10.1-1
- Update to 1.10.1

* Thu Nov 03 2016 Wim Taymans <wtaymans@redhat.com> - 1.10.0-1
- Update to 1.10.0

* Sat Oct 01 2016 Wim Taymans <wtaymans@redhat.com> - 1.9.90-1
- Update to 1.9.90

* Thu Sep 01 2016 Wim Taymans <wtaymans@redhat.com> - 1.9.2-1
- Update to 1.9.2

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.9.1-2
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Thu Jul 07 2016 Wim Taymans <wtaymans@redhat.com> - 1.9.1-1
- Update to 1.9.1

* Thu Jun 09 2016 Wim Taymans <wtaymans@redhat.com> - 1.8.2-1
- Update to 1.8.2

* Thu Apr 21 2016 Kalev Lember <klember@redhat.com> - 1.8.1-1
- Update to 1.8.1

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.6.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Dec 18 2015 Kalev Lember <klember@redhat.com> - 1.6.2-1
- Update to 1.6.2

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6.1-2
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Mon Nov 09 2015 Kalev Lember <klember@redhat.com> - 1.6.1-1
- Update to 1.6.1

* Sat Sep 26 2015 Kalev Lember <klember@redhat.com> - 1.6.0-1
- Update to 1.6.0

* Sat Aug 15 2015 Kalev Lember <klember@redhat.com> - 1.5.2-1
- Update to 1.5.2
- Use libdir macro instead of hardcoding lib64
- Use license macro for COPYING files

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun Mar  8 2015 Simon Farnsworth <simon.farnsworth@onelan.co.uk> - 1.4.0-2
- Add support for writing GStreamer plugins in Python, using upstream patch from bug report.
- BuildRequire automake for now, as patch is against autotools files

* Tue Nov 04 2014 Jon Ciesla <limburgher@gmail.com> - 1.4.0-1
- 1.4.0, BZ 1155141.

* Wed Oct  8 2014 Simon Farnsworth <simon.farnsworth@onelan.co.uk> - 1.3.90-4
- And fix 32-bit build without plugin support

* Wed Oct  8 2014 Simon Farnsworth <simon.farnsworth@onelan.co.uk> - 1.3.90-3
- Typo fix

* Wed Oct  8 2014 Simon Farnsworth <simon.farnsworth@onelan.co.uk> - 1.3.90-2
- Remove patch - it was a cherry-pick from upstream
- Disable support for writing GStreamer plugins in Python - upstream has a plugin name conflict between Python 2 and Python 3

* Wed Oct  8 2014 Simon Farnsworth <simon.farnsworth@onelan.co.uk> - 1.3.90-1
- Update to 1.3.90 upstream release

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Aug  2 2014 Simon Farnsworth <simon@farnz.org.uk> - 1.2.1-5
- Patch initialisers for post-release change in pygobject

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue May 27 2014 Kalev Lember <kalevlember@gmail.com> - 1.2.1-2
- Rebuilt for https://fedoraproject.org/wiki/Changes/Python_3.4
* Sun Apr 27 2014 Simon Farnsworth <simon.farnsworth@onelan.co.uk> - 1.2.1-1
- Upstream release gstreamer-python-1.2.1, fixing Python 3 support

* Mon Mar 17 2014 Simon Farnsworth <simon.farnsworth@onelan.co.uk> - 1.2.0-2
- Disable Python 3 support - it's too buggy to ship
- Correct faulty macro setting in Python 3 block - it broke Python 2 build

* Sun Mar 16 2014 Simon Farnsworth <simon.farnsworth@onelan.co.uk> - 1.2.0-1
- Upstream release gstreamer-python-1.2.0
- Python 3 support

* Tue Nov 12 2013 Simon Farnsworth <simon@farnz.org.uk> - 1.1.90-1
- Using the gstreamer-python specfile as an example, package gst-python for GStreamer 1.2

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10.22-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10.22-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jul 14 2012 Kalev Lember <kalevlember@gmail.com> - 0.10.22-2
- Backport gst.preset_{set,get}_app_dir(), needed for transmageddon 0.21

* Sat Jul 14 2012 Kalev Lember <kalevlember@gmail.com> - 0.10.22-1
- Update to 0.10.22 (#750016)
- Include new headers in -devel subpackage

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10.19-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10.19-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Oct 20 2010 Xavier Lamien <laxathom@fedoraproject.org> - 0.10.19-1
- Update release.

* Wed Jul 21 2010 David Malcolm <dmalcolm@redhat.com> - 0.10.16-2
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Fri Aug 14 2009 Xavier Lamien <laxathom@fedoraproject.org> - 0.10-16-1
- Update release.

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10.15-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu May 28 2009 Denis Leroy <denis@poolshark.org> - 0.10.15-1
- Update to upstream 0.10.15 (#502812)
- Added git patch to fix compile fix

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10.14-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jan 30 2009 Denis Leroy <denis@poolshark.org> - 0.10.14-1
- Update to upstream 0.10.14, with various bug fixes
- Removed problematic devel Provide

* Sat Jan 10 2009 Denis Leroy <denis@poolshark.org> - 0.10.13-1
- Update to upstraem 0.10.13
- Forked devel package with pkgconfig file (#477310)

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.10.12-2
- Rebuild for Python 2.6

* Mon Aug 25 2008 Denis Leroy <denis@poolshark.org> - 0.10.12-1
- Update to upstream 0.10.12

* Fri Mar 28 2008 Denis Leroy <denis@poolshark.org> - 0.10.11-2
- Fixed datadir directory ownership (#439291)

* Sun Mar 23 2008 Denis Leroy <denis@poolshark.org> - 0.10.11-1
- Update to upstream 0.10.11, bugfix release updates

* Wed Feb 13 2008 Denis Leroy <denis@poolshark.org> - 0.10.10-1
- Update to upstream 0.10.10, BR updates

* Sun Dec  9 2007 Denis Leroy <denis@poolshark.org> - 0.10.9-1
- Update to upstream 0.10.9
- Removed exit patch, is upstream

* Fri Sep 14 2007 Denis Leroy <denis@poolshark.org> - 0.10.8-2
- Added patch to avoid crash on exit

* Mon Aug 20 2007 Denis Leroy <denis@poolshark.org> - 0.10.8-1
- Update to upstream 0.10.8
- License tag update

* Tue Feb 20 2007 Denis Leroy <denis@poolshark.org> - 0.10.7-2
- Ship examples in doc directory only, fixes multilib conflict (#228363)
- rpmlint cleanup

* Wed Feb 14 2007 Denis Leroy <denis@poolshark.org> - 0.10.7-1
- Update to 0.10.7
- Some spec cleanups

* Mon Dec 11 2006 Denis Leroy <denis@poolshark.org> - 0.10.6-1
- Update to 0.10.6, build with python 2.5

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 0.10.5-2
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Tue Sep 26 2006 Denis Leroy <denis@poolshark.org> - 0.10.5-1
- Update to 0.10.5

* Tue Sep 19 2006 Denis Leroy <denis@poolshark.org> - 0.10.4-2
- FE Rebuild

* Thu Jun 15 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.10.4-1
- new upstream release

* Tue Jan 24 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- update to GStreamer Python Bindings 0.10.2
- remove -devel requirements

* Thu May 19 2005 Thomas Vander Stichele <thomas at apestaart dot org> - 0.8.1-6
- disable docs build - they're already in the tarball

* Tue May 10 2005 Thorsten Leemhuis <fedora[AT]leemhuis.info> - 0.8.1-5
- Move __init__.py* files from lib to _libdir on multilibarchs
  Found in thias spec file, fixes x86_64

* Fri Apr  1 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 0.8.1-3
- include missing directories

* Thu Mar 31 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 0.8.1-2
- add deps pygtk2-devel and gstreamer-devel for pkgconfig file

* Fri Dec 24 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.8.1-1: moved to Fedora Extras CVS

* Fri Dec 24 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.8.1-0.fdr.2: various cleanups

* Tue Dec 07 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.8.1-0.fdr.1: new upstream release

* Mon Nov 15 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.8.0-0.fdr.1: new upstream release

* Fri Nov 05 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.7.94-0.fdr.1: new upstream release

* Tue Oct 12 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.7.93-0.fdr.1: new upstream release

* Mon Jun 21 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.7.92-0.fdr.1: new upstream release

* Wed Mar 31 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.7.91-0.fdr.1: new upstream release

* Tue Sep 02 2003 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.1.0-0.fdr.1: first fedora release
